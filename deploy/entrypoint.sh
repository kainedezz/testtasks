#!/bin/bash
python3 manage.py migrate;
printenv | grep -v "no_proxy" >> /etc/environment;
/etc/init.d/uwsgi start;
redis-server --daemonize yes
celery --app=test_tasks worker --detach
nginx -g "daemon off;";