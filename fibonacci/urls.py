from django.conf.urls import url
from fibonacci.views import *

urlpatterns = [
    url(r'^$', TaskView.as_view()),
    url(r'^(?P<pk>[0-9]+)/$', ResultView.as_view())
]