from django.conf import settings
from rest_framework import serializers

from fibonacci.models import Task
from fibonacci import tasks


class TaskSerializer(serializers.ModelSerializer):
    """
    Task serializer
    In default returns all Task fields without result
    If context has result_field will also returns result
    """

    class Meta:
        model = Task
        read_only_fields = (
            'id', 'status', 'result',  'create_at', 'start_at', 'done_at', 'all_time', 'pending_time', 'running_time'
        )
        fields = read_only_fields + ('param',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Trying to get context
        context = kwargs.get('context')
        # If we got context and context doesn't has result_field we will delete result attr from serialization data
        if not context or not context.get('result_field'):
            self.fields.pop('result')

    @staticmethod
    def validate_param(value):
        """Validation"""

        # If value biggest then 1220 we will raise validation error
        # Max value is 1220 because in result we use CharField with max length that equal 255
        # If we provide max value biggest then 1220 result nums will slice and result will be incorrect
        if value > 1220:
            raise serializers.ValidationError('param must be in range 0-1220')

        return value

    def create(self, validated_data):
        # Creating task with param from request
        task = Task.objects.create(param=validated_data.get('param', 0))

        # If debug we will calculate result without celery
        if settings.DEBUG:
            tasks.calculate_sequence(task_id=task.id)
        else:
            tasks.calculate_sequence.delay(task.id)

        return task
