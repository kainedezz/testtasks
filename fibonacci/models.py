from django.db import models
from django.contrib.postgres.fields import ArrayField


class Task(models.Model):
    """
    Fibonacci task, that contains param, result and timer data

    Arguments:
        status:     Task status
        param:      Result sequence count
        result:     Result sequence
        create_at:  Time when task created
        start_at:   Time when celery takes a task
        done_at:    Time when task was done
    """

    class Meta:
        ordering = 'create_at',

    class Status:
        PENDING = 'pending'
        RUNNING = 'running'
        DONE = 'done'

        CHOICES = (
            (PENDING, PENDING),
            (PENDING, PENDING),
            (PENDING, PENDING)
        )

    status = models.CharField(choices=Status.CHOICES, max_length=16, default=Status.PENDING)
    param = models.PositiveIntegerField()
    result = ArrayField(
        models.CharField(max_length=255),
        null=True,
    )

    create_at = models.DateTimeField(auto_now_add=True)
    start_at = models.DateTimeField(null=True, blank=True)
    done_at = models.DateTimeField(null=True, blank=True)

    @property
    def all_time(self):
        """
        Returns (datetime.datetime): Pending time and work time
        """
        return (self.done_at - self.create_at) if self.done_at and self.create_at else None

    @property
    def pending_time(self):
        """
        Returns (datetime.datetime): Time from creation to start
        """
        return (self.start_at - self.create_at) if self.start_at and self.create_at else None

    @property
    def running_time(self):
        """
        Returns (datetime.datetime): Time from start to done
        """
        return (self.done_at - self.start_at) if self.done_at and self.start_at else None

    def calculate(self):
        """
        Method for calculate result
        """

        # Result list
        result = [1, 1]

        # This cycle will break when result will be less then task param
        while len(result) < self.param:
            # Appending to result concatenation of two last items
            result.append(result[-1] + result[-2])

        # If param is equals 0 or 1 we must slice list by param
        self.result = result[:self.param]
