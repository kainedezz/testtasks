"""
Приложение для рассчёта последовательности фибоначи

Задание:
    Нужно написать REST API для расчёта ряда чисел Фибоначчи на django-rest-framework.
    Помимо REST API понадобится сервис для хранения и обработки очереди сообщений (Redis Channels, RabbitMQ, Amazon SQS,
    любой в общем). И СУБД для хранения результатов обработки сообщений. Основное приложение выполняет 2 функции:
        - непосредственно сам REST API;
        - пул воркеров, обрабатывающих сообщения (celery, Django Q или что-то подобное).

    Это всё упаковать в docker. Для управления используется утилита docker-compose, в конфигурации которой в итоге
    имеем следующие сервисы:
        - REST API
        - Обработчик очереди
        - СУБД
        - Очередь сообщений.

    Логика работы такая:
        Задача выполняемая воркером – это расчет ряда чисел Фибоначчи.
        Параметр запуска задачи один – N уровень, до которого надо выполнить расчет.
        Первый эндпоинт API на POST запрос принимает параметр, валидирует сериализатором и добавляет задачу в очередь.
        А на GET запрос возвращает список задач, содержащий для каждой из них:
            - текущий статус;
            - дата и время начала задачи;
            - дата и время ее завершения;
            - время выполнения, которое должно быть вычисляемой дельтой в серилизаторе объекта, а не хранится в БД;
            - параметр, с которым задача запускалась.

        Второй эндпоинт, который по идентификатору задачи будет возвращать все то же, что и в списке,
        а так же результат задачи, если он имеется. В качестве результата в БД записывается полученный ряд чисел.

JetStyle
"""


from django.apps import AppConfig


class FibonacciConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fibonacci'
