import datetime
from celery import shared_task
from fibonacci import models


@shared_task
def calculate_sequence(task):
    """
    This task starts calculate task result

    Arguments:
        task (int): Task DB id
    """

    # Getting task
    task = models.Task.objects.get(id=task)

    # Changing task status to running
    task.start_at = datetime.datetime.now()
    task.status = task.Status.RUNNING
    task.save()

    # Calculating task
    task.calculate()

    # Changing task status to donned
    task.done_at = datetime.datetime.now()
    task.status = task.Status.DONE
    task.save()

    return True
