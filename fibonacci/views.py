from rest_framework.response import Response
from rest_framework import generics

from fibonacci import models
from fibonacci import serializers


class TaskView(generics.ListAPIView):
    """
    Task view
    On get returns all tasks without results
    On post creates new task
    """

    serializer_class = serializers.TaskSerializer

    def get_queryset(self):
        return models.Task.objects.all()

    def post(self, request):
        serializer = serializers.TaskSerializer(data=request.POST)

        serializer.is_valid(raise_exception=True)
        task = serializer.save()
        return Response({"task_id": task.id})


class ResultView(generics.RetrieveAPIView):
    """
    Task view
    On get returns task by id
    """

    model = models.Task
    serializer_class = serializers.TaskSerializer

    def get_serializer_context(self):
        # We need to construct serializer with this param to return result field
        return {'result_field': True}

    def get_queryset(self):
        return models.Task.objects.all()
