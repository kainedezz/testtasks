#Deploy
Creating application image from project root:
```
docker build . -f deploy/Dockerfile -t application
```
Run docker compose:
```
PG_PASS={} SECRET_KEY={} docker-compose -f deploy/docker-compose.yml up
```

######Variables:
|Variable       | Description       |
| ------------- |:-----------------:|
| PG_PASS       | Postgres password |
| SECRET_KEY    | Django secret key |

###Your`s application will be available at http://localhost:8080/  